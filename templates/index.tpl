<!-- <!doctype html> -->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/style.css" type="text/css" />
    <link rel="stylesheet" href="../css/jquery.jqplot.css" type="text/css" />
</head>

<body>

<div class="bg_container">
</div>

<div class="main_container">
<div class="logo">
  <img src="../img/logo.png"><b><span style="color: rgb(37, 33, 22);">RopeDancer</span></b>
</div>

<ul class="menu">
  <li>Home</li>
  <li class="separator">|</li>
  <li>About</li>
</ul>

<div class="top">
  <p id="balance_text">Your balance: <span id="user_balance">1500$</span></p>
</div>

<div class="middle">

  <select name="currency_pair_selector" style="width : 100px;">
    {foreach from=$symbols key=k item=v}
      <option value="{$k}" {if $selected_symbol eq $v} selected{/if}>{$v}</option>
    {/foreach}
   </select>

  Investiment
  <select name="investiment_selector" style="width : 100px;">
    {foreach from=$investiment_values key=k item=v}
      <option value="{$k}" {if $selected_investiment eq $v} selected{/if}>{$v}</option>
    {/foreach}
  </select>

  Expires in
  <select name="period_selector" style="width : 100px;">
    {foreach from=$periods key=k item=v}
      <option value="{$k}" {if $selected_period eq $v} selected{/if}>{$v}</option>
    {/foreach}
  </select>

  <div id="chart">
  </div>

  <div id="controls">
    <div id="button_call" class="call_clickable"><img src="../img/call_blue.png"></div>
    <div id="current_value">1005.50</div>
    <div id="button_put" class="put_clickable"><img id="" src="../img/put_red.png"></div>
  </div>

  <div class="vertical_separator">
  </div>

  <div class="launch_block">
    <p>
      Potential Payout <span id="potential_payout">170$</span>
    </p>
    <p>
      In <span id="period">60 Seconds</span>
    </p>
    <p id="payout_percent"><b>70%</b></p>
    <p id="payout_return">Return</p>
    <div id="button_start" class="bg_zero">Start</div>
    <span id="time_left"></span>
  </div>

</div>

<div class="bottom">
  Developed by <b>PoetInTheCode</b> <b>©</b> 2012
</div>

</div>

    <script type="text/javascript" src="../scripts/jquery.min.js"></script>
    <script type="text/javascript" src="../scripts/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="../scripts/excanvas.min.js"></script>
    <script type="text/javascript" src="../scripts/jqplot.highlighter.min.js"></script>
    <script type="text/javascript" src="../scripts/jqplot.cursor.min.js"></script>
    <script type="text/javascript" src="../scripts/jqplot.dateAxisRenderer.min.js"></script>
    <script type="text/javascript">
      window.period = '{$selected_period}';
      window.investiment = '{$selected_investiment}';
      window.symbol = '{$selected_symbol}';
      window.user_balance = '{$user_balance}';
    </script>
    <script type="text/javascript" src="../scripts/app.js"></script>
    
</body>
</html>