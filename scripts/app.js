//
// Main application javascript
//

function getChartDataAndRedraw() {
  $.ajax({
    url: "/getchartsdata/?p=100&s="+window.symbol.replace('/', ''),
    context: document.body
  }).done(function(response) { 
    updateChart(JSON.parse(response));
  });
}

function updateChart(rawChartDataArray) {
    var chartDataArray = [];
    $.each(rawChartDataArray, function(index, value) {
      chartDataArray.push([50-index, Number(value.value)]);
    });
    chartDataArray.reverse();
    redrawCurrentValue(chartDataArray[chartDataArray.length-1][1], chartDataArray[chartDataArray.length-2][1]);
    window.current_value = chartDataArray[chartDataArray.length-1][1];
    redrawButtonStart();
    updateSelectors();
    $('#user_balance').text(window.user_balance+'$');
    $('#period').text(window.period);
    $('#potential_payout').text(parseInt(window.investiment)+parseInt(parseInt(window.investiment)*70/100) + '$');
    if(typeof window.plot == 'undefined') {  
      firstInitializeChart(chartDataArray);
      return;
    }
    // chartDataArray.reverse();
    window.plot.series[0].data = chartDataArray;
    window.plot.title.text = 'WILL '+window.symbol+' GO UP OR DOWN?';
    window.plot.replot( { resetAxes: true } );
}

function updateSelectors() {
  if(typeof window.rateTimer !== "undefined") {
    $('select[name="currency_pair_selector"]').prop('disabled', true);
    $('select[name="period_selector"]').prop('disabled', true);
    $('select[name="investiment_selector"]').prop('disabled', true);
  } else {
    $('select[name="currency_pair_selector"]').prop('disabled', false);
    $('select[name="period_selector"]').prop('disabled', false);
    $('select[name="investiment_selector"]').prop('disabled', false);
  }
 }

function firstInitializeChart(chartDataArray) {
  window.plot = $.jqplot('chart', [chartDataArray], {
    title: 'WILL '+window.symbol+' GO UP OR DOWN?',
    axes:{
      xaxis: {
        pad: 0
      },
      yaxis:{
        tickOptions:{
          formatString:'%.5f'
        }
      }
    },
    highlighter: {
      show: true
    },
    series: [
        {
        markerOptions: {
            size: 0
        }}              
    ],
    cursor: {
      show: true,
      tooltipLocation:'sw'
    },
    autoscale: true
  });
}

function redrawCurrentValue(currentValue, previosValue) {
  $('#current_value').removeClass('zero');
  $('#current_value').removeClass('call');
  $('#current_value').removeClass('put');
  if(currentValue - previosValue > 0) {
    $('#current_value').addClass('call');
  }
  if(currentValue - previosValue < 0) {
    $('#current_value').addClass('put');
  }
  if(currentValue - previosValue == 0) {
    $('#current_value').addClass('zero');
  }
  $('#current_value').text(currentValue);
}

function redrawButtonStart() {
  $('#button_start').removeClass('bg_call');
  $('#button_start').removeClass('bg_put');
  $('#button_start').removeClass('bg_zero');
  $('#button_start').removeClass('start_clickable');
  $('#button_call').addClass('call_clickable');
  $('#button_put').addClass('put_clickable');
   $('#time_left').css('display', 'none');
  if(typeof window.rateTimer !== "undefined") {
    $('#current_value').html($('#current_value').text()+'<br>(start:'+window.start_currency+')');
    $('#button_start').removeClass('start_clickable');
    $('#button_call').removeClass('call_clickable');
    $('#button_put').removeClass('put_clickable');
    $('#time_left').css('display', 'block');
  } else {
    if((window.prediction == 'call')||(window.prediction == 'put')) {
      $('#button_start').addClass('start_clickable');
    }
  }
  if(window.prediction == 'call') {
    $('#button_start').addClass('bg_call');
    return;
  }
  if(window.prediction == 'put') {
    $('#button_start').addClass('bg_put');
    return;
  }
  $('#button_start').addClass('bg_zero');
}

String.prototype.toMMSS = function () {
    sec_numb    = parseInt(this);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    if(minutes != 0) {
      var time    = 'Time left: '+minutes +' min '+seconds +' sec';
    } else {
      var time    = 'Time left: '+seconds +' sec';
    }
    return time;
}

String.prototype.toMMSSGraph = function () {
    sec_numb    = parseInt(this);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = minutes+':'+seconds;
    return time;
}

function recalculateBalance(finish_currency) {
  console.log('finish_currency: '+finish_currency);
  console.log('start_currency: '+start_currency);

  if(Number(finish_currency) - Number(window.start_currency) > 0) {
    if(window.prediction === 'call') {
      window.user_balance += Number($('#potential_payout').text());
    }
    if(window.prediction === 'put') {
      window.user_balance -= Number($('#potential_payout').text());
    }
  }
  if(Number(finish_currency) - Number(window.start_currency) < 0) {
    if(window.prediction === 'call') {
      window.user_balance -= Number($('#potential_payout').text());
    }
    if(window.prediction === 'put') {
      window.user_balance += Number($('#potential_payout').text());
    }
  }
  if(window.user_balance < 0) {
    window.user_balance = 0;
  }
}

$(document).ready(function(){
  setInterval(getChartDataAndRedraw, 5000);
  getChartDataAndRedraw();

  $('select[name="currency_pair_selector"]').change(function(value){
    var text = $("select[name=currency_pair_selector] option:selected").text();
    window.symbol = text;
    getChartDataAndRedraw();
  });
  $('select[name="period_selector"]').change(function(value){
    var text = $("select[name=period_selector] option:selected").text();
    window.period = text;
    getChartDataAndRedraw();
  });
  $('select[name="investiment_selector"]').change(function(value){
    var text = $("select[name=investiment_selector] option:selected").text();
    window.investiment = text;
    getChartDataAndRedraw();
  });
  $('.call_clickable').live('click', function(){
    window.prediction = 'call';
    getChartDataAndRedraw();
  });
  $('.put_clickable').live('click', function(){
    window.prediction = 'put';
    getChartDataAndRedraw();
  });
  $('.start_clickable').live('click', function() {
    window.rateTimer = setTimeout(
      function() {
          recalculateBalance(window.current_value);
          window.prediction = '';
          getChartDataAndRedraw();
          clearInterval(window.onSecTimer);
          clearTimeout(window.rateTimer);
          delete window.onSecTimer;
          delete window.rateTimer;
      },
      parseInt(window.period)*60000
    );
    window.timeLeftInSec = parseInt(window.period)*60;
    window.onSecTimer = setInterval(function(){
        window.timeLeftInSec -= 1;
        $('#time_left').text(window.timeLeftInSec.toString(10).toMMSS());
    }, 1000);
    window.start_currency = $('#current_value').text();
    getChartDataAndRedraw();
  });

});