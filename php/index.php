<?php
	
	require_once('../lib/Smarty-3.1.12/Smarty.class.php');

	spl_autoload_register(function ($classname) {
	    require_once("/var/www/php/{$classname}.php");
	});	

	$app = new Application($_GET['route']); 
	$app->execute();

?>