<?php

class Router 
{
	
	private $rules = array();
	private $url = '';

	function __construct($url, $rules)
	{
		$this->url = $url;
		if($url == '') {
			$this->url = '/index/';
		}
		$this->rules = $rules;
	}

	public function doRoute() {
		$destination = $this->findDestination();
		$this->launchScript($destination);
	}

	private function findDestination() {
		foreach($this->rules as $urlPattern => $destinationAssoc) {
			preg_match($urlPattern, $this->url, $matches);
			if (count($matches) > 0) {
				return $destinationAssoc;
			}
		}

		throw new Exception("Error 404: Page not found", 0);
	}

	private function launchScript($destinationAssoc) {
		include_once($destinationAssoc['class'].'.php');
	}
}

?>