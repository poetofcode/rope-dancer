<?php

class Templater
{

	private $smarty = null;

	function __construct() {
		$this->setupTemplateEngine();
	}

	private function setupTemplateEngine() {
		$this->smarty = new Smarty();     
		$this->smarty->template_dir = '../templates';
		$this->smarty->compile_dir  = '../templates_c';  
	}

	public function assign($name, $value) {
		$this->smarty->assign($name, $value);  		
	}

	public function display($templatePath) {
		$this->smarty->display($templatePath);
	}
}

?>