<?php

class DBCurrencyCollector implements IteratorAggregate
{
	
	private $currencyRows = array();
	private $dbConnection = null;

	function __construct($dbConnection)
	{
		$this->dbConnection = $dbConnection;			
	}

	public function fetchCurrencies($fetchConditions) {
		$sth = $this->dbConnection->prepare('SELECT value, time, timestamp FROM currencies WHERE symbol LIKE :symbol ORDER BY id DESC LIMIT :periods');
		$sth->execute(array(':symbol' => $fetchConditions['symbol'], ':periods' => $fetchConditions['periods']));
		$this->currencyRows = $sth->fetchAll(PDO::FETCH_ASSOC);
	} 

    public function getIterator() {  
        return new ArrayIterator($this->currencyRows);  
    }  

}

?>