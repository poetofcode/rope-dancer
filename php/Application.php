<?php

class Application
{
	private $url = "";

	function __construct($url = "")
	{
		$this->url = $url;

	}

	public function execute() {
		try {
			$this->configureDatabase();
			$rules = $this->getRouterRules();
			$router = new Router($this->url, $rules);
			$router->doRoute();
		} catch(Exception $e) {
			echo($e->getMessage());
		}
	} 

	public function configureDatabase() {
		$configurator = new MySQLConnectionConfigurator();
		$configurator->configureConnection();
	}

	private function getRouterRules() {
		$rulesAssoc = array(
			'/index/' => array(
				'class' => 'HomePage'
			),
			'/getchartsdata/' => array(
				'class' => 'ChartsData'
			),
			'/gatheringcurrencies/' => array(
				'class' => 'GrabberScript'
			),
		);
		
		return $rulesAssoc;
	}
}

?>