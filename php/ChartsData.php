<?php

$collector = new DBCurrencyCollector(MySQLConnectionWrapper::getInstance()->getConnection());
$collector->fetchCurrencies(array('symbol' => isset($_GET['s']) ? $_GET['s'] : 'USDEUR', 
		'periods' => isset($_GET['p']) ? $_GET['p'] : 10));

echo(json_encode(iterator_to_array($collector)));

?>