<?php

	$templater = new Templater();

	$symbols = array('USD/EUR', 'EUR/GBP', 'GBP/USD');
	$templater->assign('symbols', $symbols);
	$templater->assign('selected_symbol', 'EUR/GBP');

	$investiment_values = array('5$', '10$', '20$', '50$', '100$', '250$', '500$');
	$templater->assign('investiment_values', $investiment_values);
	$templater->assign('selected_investiment', '10$');

	$periods = array('1 minute', '5 minutes', '10 minutes', '30 minutes');
	$templater->assign('periods', $periods);
	$templater->assign('selected_period', '5 minutes');
	
	$templater->assign('user_balance', 1500);
	
	$templater->display('index.tpl');

?>