<?php

class MySQLConnectionWrapper
{
    protected static $instance;
	private $user = "";
	private $pass = "";
	private $baseName = "";
	private $hostName = "";
	private $port = 0;
	private $dbConnection = null;

    private function __construct(){
    }

    private function __clone(){
    }

    private function __wakeup(){
    }

    public static function getInstance() {
        if ( is_null(self::$instance) ) {
            self::$instance = new MySQLConnectionWrapper;
        }
        return self::$instance;
    }

	function __destruct() {
		unset($this->dbConnection);
	}

	public function configureDB($user, $pass, $hostName = "127.0.0.1", $port = 3306, $baseName = "test") {
		if($this->dbConnection) {
			return;
		}
		$this->user = $user;
		$this->pass = $pass;
		$this->baseName = $baseName;
		$this->hostName = $hostName;
		$this->port = $port;
		$this->createDBConnection();
	}

	public function getConnection() {
		return $this->dbConnection;
	}

	private function createDBConnection() {
		if($this->dbConnection) {
			// logging: DB-connection already exists
			return;
		}
		try {
		    $this->dbConnection = new PDO("mysql:dbname=$this->baseName;host=$this->hostName;port=$this->port;charset=utf8", 
		    	$this->user, $this->pass);
			$this->dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);		
		} catch (PDOException $e) {
			unset($this->dbConnection);
			$this->dbConnection = null;
		    print "<br/>" . "Error!: " . $e->getMessage() . "<br/>";
		}
	}

}


?>