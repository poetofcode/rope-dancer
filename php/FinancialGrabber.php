<?php

class FinancialGrabber
{

	private $dbConnection = null;
	private $externalDataResourceLink = 'http://www.xe.com/';

	function __construct($dbConnection) {
		$this->dbConnection = $dbConnection;
	}

	public function retrieveData() {
		try {
			$data = $this->parseData();
			$this->saveData($data);
			echo('ok');
		} catch (Exception $e) {
			// logging exception error info
			echo('failed');
			echo('<br>'.$e->getMessage());
		}
	}

	private function parseData() {
		$financialDataAssoc = array();

        $ch = curl_init($this->externalDataResourceLink);

        $response = "";
        $callback = function($url, $chunk) use (&$response){
            $response .= $chunk;
            return strlen($chunk);
        };
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent:Mozilla/5.0'));
        curl_setopt($ch, CURLOPT_WRITEFUNCTION, $callback);
        curl_exec($ch);
        curl_close($ch);
        
        // $response = mb_convert_encoding($response, 'utf-8', 'windows-1251');
        $response = str_replace(array("\n", "\r"), ' ', $response);
        
		preg_match(
		  '/<a[^>]*rel="EUR,USD,1,2"[^>]*>(.*?)<\/a>/',
		  $response, $eur_usd_cell_content
		);
		preg_match(
		  '/<a[^>]*rel="GBP,EUR,2,3"[^>]*>(.*?)<\/a>/',
		  $response, $eur_gbp_cell_content
		);
		preg_match(
		  '/<a[^>]*rel="USD,GBP,3,1"[^>]*>(.*?)<\/a>/',
		  $response, $gbp_usd_cell_content
		);
		preg_match(
		  '/<span[^>]*id="ratesTimestamp"[^>]*>(.*?)<\/span>/',
		  $response, $rates_timestamp_content
		);
		
		$clean_str_timestamp = substr($rates_timestamp_content[1], 2);
		$date = new DateTime();
		$numeric_timestamp = (string)$date->getTimestamp();

		$financialDataAssoc['USDEUR'] = array('value' => $eur_usd_cell_content[1], 
			'time' => $clean_str_timestamp, 
			'timestamp' => $numeric_timestamp);
		$financialDataAssoc['EURGBP'] = array('value' => $eur_gbp_cell_content[1], 
			'time' => $clean_str_timestamp,
			'timestamp' => $numeric_timestamp);
		$financialDataAssoc['GBPUSD'] = array('value' => $gbp_usd_cell_content[1], 
			'time' => $clean_str_timestamp,
			'timestamp' => $numeric_timestamp);

		return $financialDataAssoc;
	}

	private function saveData($financialDataAssoc) {
		$stmt = $this->dbConnection->prepare('INSERT INTO currencies (symbol, value, time, timestamp) VALUES(:symbol, :value, :time, :timestamp)');
		foreach ($financialDataAssoc as $symbol => $otherParams) {
			$stmt->execute(array(':symbol' => $symbol,
				':value' => $otherParams['value'], 
				':time' => $otherParams['time'],
				':timestamp' => $otherParams['timestamp']
			));
		}
	}
}

?>